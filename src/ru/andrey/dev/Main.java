package ru.andrey.dev;

import java.io.IOException;

/**
 * Демонстрационный класс {@code Main}.
 *
 * @author Андрей Рыжкин
 * @version 1.0
 * @since 02.11.2016
 */
public class Main {
    public static void main(String[] args) throws IOException {
        CreateFile.recordFile("intdata.dat");
        SixDigitNumbers.roomSelection("intdata.dat", "int6data.dat");
        HappyRoom.recordFile("int6data.dat", "text6data.dat");
    }
}
