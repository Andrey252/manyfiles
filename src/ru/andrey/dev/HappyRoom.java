package ru.andrey.dev;

import java.io.*;

/**
 * Класс {@code HappyRoom} отбирает из указанного файла номера счастливых билетов и записывает в другой указанный файл.
 *
 * @author Андрей Рыжкин.
 * @version 1.0
 * @since 02.11.2016
 */
public class HappyRoom {
    /**
     * Метод {@code recordFile}отбирает из указанного файла номера счастливых билетов и записывает в другой указанный файл,
     * бросая исключения: если невозможно прочитать исходный файл, ошибка при создании выходного файла, неверно указан
     * путь к исходному или выходному файлу.
     *
     * @param pathInputFile  путь исходного файла.
     * @param pathOutputFile путь выходного файла.
     * @throws IOException если невозможно прочитать исходный файл, ошибка при создании выходного файла, неверно указан
     *                     путь к исходному или выходному файлу.
     */
    public static void recordFile(String pathInputFile, String pathOutputFile) throws IOException {

        BufferedReader inputData = new BufferedReader(new FileReader(pathInputFile));
        BufferedWriter outData = new BufferedWriter(new FileWriter(pathOutputFile));
        String str;

        int number;
        while ((str = inputData.readLine()) != null) {
            number = Integer.parseInt(str);
            if (thisLuckyNumber(number)) {
                outData.write(str + "\n");
            }
        }
        inputData.close();
        outData.close();
    }

    /**
     * Возвращает логическое значение {@code boolean}, проверяя шестизначный номер билета (счастливый или нет).
     *
     * @param number номер билета.
     * @return {@code true} счастливый билет, иначе {@code false}.
     */
    public static boolean thisLuckyNumber(int number) {
        int sumTheFirstThree = 0;
        int sumTheSecondThree = 0;
        for (int i = 0; i < 3; i++) {
            sumTheFirstThree += number % 10;
            number /= 10;
        }
        for (int i = 0; i < 3; i++) {
            sumTheSecondThree += number % 10;
            number /= 10;
        }
        if (sumTheFirstThree == sumTheSecondThree) {
            return true;
        } else {
            return false;
        }
    }

}
