package ru.andrey.dev;

import java.io.*;

/**
 * Класс {@code SixDigitNumbers} отбирает из указанного файла шестизначные номера и записывает в другой указанный файл.
 *
 * @version 1.0
 * @author Андрей Рыжкин.
 * @since 02.11.2016
 */
public class SixDigitNumbers {
    /**
     * Метод {@code roomSelection} отбирает из указанного файла шестизначные номера и записывает в другой указанный
     * файл. Если в исходном файле номер билета имеет диапозон: от 4 до 5 значного, то к началу номера билета дописываются нули,
     * превращая его в шестизначный.
     *
     * @param pathInputFile  путь исходного файла, содержащий номера билетов.
     * @param pathOutputFile путь выходного файла.
     * @throws IOException если невозможно прочитать исходный файл, ошибка при создании выходного файла, неверно указан
     *                     путь к исходному или выходному файлу.
     */
    public static void roomSelection(String pathInputFile, String pathOutputFile) throws IOException {
        BufferedReader inputData = new BufferedReader(new FileReader(pathInputFile));
        BufferedWriter outData = new BufferedWriter(new FileWriter(pathOutputFile));
        String str;

        while ((str = inputData.readLine()) != null) {
            switch (str.length()) {
                case 6:
                    outData.write(str + "\n");
                    break;
                case 5:
                    outData.write("0".concat(str) + "\n");
                    break;
                case 4:
                    outData.write("00".concat(str) + "\n");
                    break;
            }
        }
        inputData.close();
        outData.close();
    }

}
