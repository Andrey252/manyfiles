package ru.andrey.dev;

import java.io.*;

/**
 * Created by Андрей on 02.11.2016.
 */
public class CheckFiles {
    /**
     * Возращает логическое значение {@code boolean}, проверяя исходный и выходной файл по указанному пути,
     * бросая исключения: неверно указан путь к файлу, файл защищён от записи, пустой исходный файл.
     *
     * @return {@code true} файлы не содержат ошибки, иначе {@code false}.
     * @throws IOException          если неверно указан путь к файлу, файл защищён от записи, пустой исходный файл.
     * @throws NullPointerException если пустой исходный файл.
     */
    static boolean check(String inputPathFile, String outputPathFile) throws IOException {
        BufferedReader inputData = null;
        BufferedWriter outputData = null;
        try {
            inputData = new BufferedReader(new FileReader(inputPathFile));
        } catch (IOException e) {
            throw new FileNotFoundException(e + inputPathFile);
        }
        if (inputData.readLine() == null) {
            throw new NullPointerException("Пустой исходный файл " + inputPathFile);
        }
        try {
            outputData = new BufferedWriter(new FileWriter(outputPathFile));
        } catch (IOException e) {
            throw new FileNotFoundException(e + outputPathFile);
        }
        inputData.close();
        outputData.close();
        return true;

    }
}
