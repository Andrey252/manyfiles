package ru.andrey.dev;

import java.io.*;
import java.util.Random;

/**
 * Класс {@code CreateFile} создаёт указанный файл и записывает рандомные числа: от 4 до 7 значных.
 *
 * @author Андрей Рыжкин.
 * @version 1.0
 * @since 02.11.2016
 */
public class CreateFile {
    /**
     * Метод {@code recordFile} записывает в указанный файл рандомные числа: от 4 до 7 значных, бросая исключение,
     * если файл защищён от записи или неверно указан путь к файлу.
     *
     * @param filePath путь файла
     * @throws IOException если диск защищён от записи файла.
     */
    public static void recordFile(String filePath) throws IOException {
        BufferedWriter outData = new BufferedWriter(new FileWriter(filePath));
        Random rand = new Random();
        int length;
        int array[];

        length = 6 + rand.nextInt(200);
        array = new int[length];
        for (int i = 0; i < length; i++) {
            array[i] = rand.nextInt(1111111);
            outData.write(Integer.toString(array[i]) + "\r\n");
        }
        outData.close();
    }
}

